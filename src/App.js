import React, {Component} from 'react';
import Projects from './components/Projects';
import AddProject from './components/AddProject';
import uuidv4 from 'uuid/v4';

class App extends Component {
    // Step 1 : Create state
    constructor() {
        super();
        this.state = {
            projects: []
        }
    }

    componentWillMount() {
        this.setState({
            projects: [
                {
                    id: uuidv4(),
                    title: "LearnEra",
                    category: "Android"
                },
                {
                    id: uuidv4(),
                    title: "SpeeBuy",
                    category: "React"
                },
                {
                    id: uuidv4(),
                    title: "SpeeBuy-Desktop",
                    category: "Electron"
                }
            ]
        })
    }

    handleAddProject(project){
        let newProjects = this.state.projects;
        newProjects.push(project);
        this.setState({projects: newProjects});
    }

    handleDeleteProject(id){
        let projects = this.state.projects;
        let index = projects.findIndex(x => x.id === id);
        projects.splice(index, 1);
        this.setState({projects});
    }

    render() {
        return (
            <div>
                <AddProject addProject={this.handleAddProject.bind(this)}/>
                <Projects projects={this.state.projects} onDelete={this.handleDeleteProject.bind(this)}/>
            </div>
        );
    }
}

export default App;
